package net.conradbets.androidapi;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import net.conradbets.androidapi.models.User;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Main activity class.
 */
public class MainActivity extends AppCompatActivity {

    public static final String API_URL = "https://conradbets.azurewebsites.net/api/users";

    /**
     * The profile image view.
     */
    private ImageView imageView;

    /**
     * The first name Text view.
     */
    private TextView firstNameTextView;

    /**
     * The last name text view.
     */
    private TextView lastNameTextView;

    /**
     * The title text view.
     */
    private TextView titleTextView;

    /**
     * The URL text view.
     */
    private TextView urlTextView;

    /**
     * The user id edit text view.
     */
    private EditText userIdEditText;

    /**
     * The search button view.
     */
    private Button searchButton;

    /**
     * The HTTP request queue.
     * This field is global since it is declared in the class and not in a method.
     * This means, any method can access this object.
     */
    private RequestQueue mRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set the views
        imageView = findViewById(R.id.imageView);
        firstNameTextView = findViewById(R.id.firstNameTextView);
        lastNameTextView = findViewById(R.id.lastNameTextView);
        titleTextView = findViewById(R.id.titleTextView);
        urlTextView = findViewById(R.id.urlTextView);
        userIdEditText = findViewById(R.id.userIdEditText);
        searchButton = findViewById(R.id.searchButton);

        // Set the button onclick listener.
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //call the getUser method when the button is pressed/clicked.
                getUser(userIdEditText.getText().toString().trim());

                //hide the keyboard
                View view = MainActivity.this.getCurrentFocus(); //get current view in focus
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null) {
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                }
            }
        });

        // Instantiate the RequestQueue.
        mRequestQueue = Volley.newRequestQueue(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Make api request to create new user.
     */
    private void postUser(User user) {
        JSONObject object;
        try {
            Gson gson = new Gson();
            object = new JSONObject(gson.toJson(user));  //convert User object into JSONObject
        } catch (JSONException e) {
            // something went wrong, try debugging
            Toast.makeText(MainActivity.this,  e.getMessage(), Toast.LENGTH_SHORT).show();
            return;
        }

        //create json object request
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, API_URL, object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //success!
                Gson gson = new Gson();
                User user = gson.fromJson(response.toString(), User.class);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(MainActivity.this, "Unable to create user. Status code -" + error.networkResponse.statusCode, Toast.LENGTH_SHORT).show();
            }
        });

        // Add the request to the RequestQueue.
        mRequestQueue.add(request);
    }

    /**
     * Make api request to retrieve existing user.
     * @param id the user id.
     */
    private void getUser(String id) {
        //create json request.
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, API_URL + "/" + id, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //success!
                Gson gson = new Gson();
                User user = gson.fromJson(response.toString(), User.class);

                //set view info
                if (!TextUtils.isEmpty(user.getPictureUrl())) {
                    Picasso.get().load(user.getPictureUrl()).into(imageView);
                }
                firstNameTextView.setText(user.getFirstName());
                lastNameTextView.setText(user.getLastName());
                titleTextView.setText(user.getPosition());
                urlTextView.setText(user.getConradProfile());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null) {
                    Toast.makeText(MainActivity.this, "Unable to retrieve user. Status code - " + error.networkResponse.statusCode, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(MainActivity.this, "Unable to retrieve user. - " + error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        // Add the request to the RequestQueue.
        mRequestQueue.add(request);
    }

    /**
     * Make api request to retrieve existing users.
     */
    private void getUsers() {
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, API_URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //success!
                Gson gson = new Gson();
                Type listType = new TypeToken<List<User>>(){}.getType();
                List<User> users = gson.fromJson(response.toString(), listType);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null) {
                    Toast.makeText(MainActivity.this, "Unable to retrieve user. Status code - " + error.networkResponse.statusCode, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(MainActivity.this, "Unable to retrieve user. - " + error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        // Add the request to the RequestQueue.
        mRequestQueue.add(request);
    }

    /**
     * Make api request to update existing user.
     * @param user the user to update.
     */
    private void putUser(User user) {
        JSONObject object;
        try {
            //convert user object into JSONObject
            Gson gson = new Gson();
            object = new JSONObject(gson.toJson(user));
        } catch (JSONException e) {
            // an error occurred - try debugging application.
            Toast.makeText(MainActivity.this,  e.getMessage(), Toast.LENGTH_SHORT).show();
            return;
        }

        //create json object request
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.PUT, API_URL + "/" + user.getId(), object, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //success!
                Gson gson = new Gson();
                User user = gson.fromJson(response.toString(), User.class);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null) {
                    Toast.makeText(MainActivity.this, "Unable to update user. Status code - " + error.networkResponse.statusCode, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(MainActivity.this, "Unable to update user. - " + error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        // Add the request to the RequestQueue.
        mRequestQueue.add(request);
    }

    /**
     * Make Api request to delete user.
     * @param id the user id
     */
    private void deleteUser(String id) {
        //create json request.
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.DELETE, API_URL + "/" + id, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                //success!
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.networkResponse != null) {
                    Toast.makeText(MainActivity.this, "Unable to delete user. Status code - " + error.networkResponse.statusCode, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(MainActivity.this, "Unable to delete user. - " + error.getMessage(), Toast.LENGTH_LONG).show();
                }
            }
        });

        // Add the request to the RequestQueue.
        mRequestQueue.add(request);
    }
}
