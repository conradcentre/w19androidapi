package net.conradbets.androidapi.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * User model class.
 * @author Eyram Dornor on 1/2/2019.
 */
public class User {

    @Expose
    @SerializedName("id")
    private String id;

    @Expose
    @SerializedName("last_name")
    private String lastName;

    @Expose
    @SerializedName("first_name")
    private String firstName;

    @Expose
    @SerializedName("conrad_profile")
    private String conradProfile;

    @Expose
    @SerializedName("email")
    private String email;

    @Expose
    @SerializedName("personal_site")
    private String personalSite;

    @Expose
    @SerializedName("groups")
    private String groups;

    @Expose
    @SerializedName("location")
    private String location;

    @Expose
    @SerializedName("pic_url")
    private String pictureUrl;

    @Expose
    @SerializedName("position")
    private String position;

    /**
     * Constructor.
     */
    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getConradProfile() {
        return conradProfile;
    }

    public void setConradProfile(String conradProfile) {
        this.conradProfile = conradProfile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPersonalSite() {
        return personalSite;
    }

    public void setPersonalSite(String personalSite) {
        this.personalSite = personalSite;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(String groups) {
        this.groups = groups;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }
}

